#include <iostream>
#include <stdlib.h>
#include "binary_search.h"
//2015004057 김범수
using namespace std;

int main(void)
{
    BinarySearch *binarySearch = NULL;
    
    while(true)
    {
        if(binarySearch == NULL) // input array
        {
            string inputs;
            getline(cin, inputs);
            
            string *argv = new string[inputs.length()]; // more than count
            string token;
            size_t pos;
            int argc = 0;
            
            while(true)
            {
                pos = inputs.find(' ');
                token = inputs.substr(0, pos);
                
                argv[argc++] = token;
                
                if(pos >= inputs.length()) break;
                else inputs.erase(0, pos + 1);
            }
            
            int *elements = NULL;
            int elementCount = 0;
            
            for(int i=0; i<argc; ++i)
            {
                string arg = argv[i];

                if(arg == "{" && elements == NULL) elements = new int[argc];
                else if(isdigit(arg[arg.length() - 1])) elements[elementCount++] = atoi(arg.c_str());
                else if(arg == "}")
                {
                    binarySearch = new BinarySearch(elements, elementCount);
                    
                    delete elements;
                    elements = NULL;
                    elementCount = 0;
                }
            }
        }
        else // input element to find
        {
            int element;
            cin >> element;
            
            if(element < -998) break;
            
            cout << binarySearch->getIndex(element) << endl;
        }
    }
    
    delete binarySearch;
    return 0;
}

BinarySearch::BinarySearch(){}

BinarySearch::BinarySearch(int *_array, int _arrayCount){
	mArray=new int[_arrayCount];
	for(int i=0;i<_arrayCount;++i)	mArray[i]=_array[i];
	mArrayCount=_arrayCount;	
}

BinarySearch::~BinarySearch(){}

void BinarySearch::sortArray(){
	int i,j,temp;
	for(i=0;i<mArrayCount-1;++i){
		for(j=i+1;j<mArrayCount;++j){
			if(mArray[i]>mArray[j]){
				temp=mArray[i];
				mArray[i]=mArray[j];
				mArray[j]=temp;
			}
		}
	}	
}
int BinarySearch::getIndex(int _element){ // return -1 when element doesn't exist
	sortArray();
	int start=0,end=mArrayCount-1;
	for(int i=0;i<100;++i){
		if(_element>mArray[(start+end)/2])	start=(start+end)/2;
		else if(_element<mArray[(start+end)/2])	end=(start+end)/2;
		else	return (start+end)/2;
		if(end==mArrayCount-1 && start==end-1)	start=end;
	}
	return -1;
}