// simple_int_set.h
// Implement your simple_int_set.cc
//2015004057 김범수

#ifndef __hw03__simple_int_set__
#define __hw03__simple_int_set__

#define MAX_SIZE 1024
#include <iostream>

using namespace std;

class SimpleIntSet
{
private:
    int *mElements;
    int mElementCount;

    void sortElements(); // you can reuse your previous sort assignment
    SimpleIntSet();
    
public:
    SimpleIntSet(int *_elements, int _count);
    ~SimpleIntSet();
    
    int *elements() const; // return sorted array
    int elementCount() const;
    
    SimpleIntSet *unionSet(SimpleIntSet& _operand);
    SimpleIntSet *differenceSet(SimpleIntSet& _operand);
    SimpleIntSet *intersectSet(SimpleIntSet& _operand);
    
    void printSet();
};

#endif

SimpleIntSet::SimpleIntSet(){}

SimpleIntSet::SimpleIntSet(int *_elements, int _count){
	mElements = new int[_count];
	for(int i=0;i<_count;++i)	mElements[i]=_elements[i];
	mElementCount=_count;
}

SimpleIntSet::~SimpleIntSet(){}

void SimpleIntSet::sortElements(){
	int i,j,temp;
	for(i=0;i<mElementCount-1;++i){
		for(j=i+1;j<mElementCount;++j){
			if(mElements[i]>mElements[j]){
				temp=mElements[i];
				mElements[i]=mElements[j];
				mElements[j]=temp;
			}
		}
	}	
}

int SimpleIntSet::elementCount() const{
	return mElementCount;
}

SimpleIntSet* SimpleIntSet::unionSet(SimpleIntSet& _operand){
	int newcount=mElementCount,number=mElementCount;	//두 집합을 더했을때, 새로 만들어 지는 집합의 원소의 갯수를 담는 변수인 newcount
	for(int j=0;j<_operand.mElementCount;++j){
		for(int i=0;i<mElementCount;++i){
			if(_operand.mElements[j]==mElements[i])	break;
			else if(i==mElementCount-1)	++newcount;
		}
	}
	int* elements = new int[newcount];
	for(int i=0;i<mElementCount;++i)	elements[i]=mElements[i];
	for(int j=0;j<_operand.mElementCount;++j){
		for(int i=0;i<mElementCount;++i){
			if(_operand.mElements[j]==mElements[i])	break;
			else if(i==mElementCount-1)	elements[number++]=_operand.mElements[j];
		}
	}
	SimpleIntSet* temp = new SimpleIntSet(elements,newcount);
	delete[] elements;
	temp->sortElements();
	return temp;
}

SimpleIntSet* SimpleIntSet::differenceSet(SimpleIntSet& _operand){
	int newcount=mElementCount,number=0;	//두 집합을 뺐을때, 새로 만들어 지는 집합의 원소의 갯수를 담는 변수인 newcount
	for(int i=0;i<mElementCount;++i){
		for(int j=0;j<_operand.mElementCount;++j){
			if(mElements[i]==_operand.mElements[j]){
				--newcount;
				break;
			}
		}
	}
	int* elements = new int[newcount];
	for(int i=0;i<mElementCount;++i){
		for(int j=0;j<_operand.mElementCount;++j){
			if(mElements[i]==_operand.mElements[j])	break;
			else if(j==_operand.mElementCount-1)	elements[number++]=mElements[i];
		}
	}
	SimpleIntSet* temp = new SimpleIntSet(elements,newcount);
	delete[] elements;
	temp->sortElements();
	return temp;
}

SimpleIntSet* SimpleIntSet::intersectSet(SimpleIntSet& _operand){
	int newcount=0,number=0;
	for(int i=0;i<mElementCount;++i){
		for(int j=0;j<_operand.mElementCount;++j){
			if(mElements[i]==_operand.mElements[j]){
				++newcount;
				break;
			}
		}
	}
	int* elements = new int[newcount];
	for(int i=0;i<mElementCount;++i){
		for(int j=0;j<_operand.mElementCount;++j){
			if(mElements[i]==_operand.mElements[j]){
				elements[number++]=mElements[i];
				break;
			}
		}
	}
	SimpleIntSet* temp = new SimpleIntSet(elements,number);
	delete[] elements;
	temp->sortElements();
	return temp;
}

void SimpleIntSet::printSet(){
	cout << "{ ";
	for(int i=0;i<mElementCount;++i)	cout << mElements[i] << " ";
	cout <<"}" << endl;
}