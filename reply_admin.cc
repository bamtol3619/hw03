//2015004057 김범수
#include <iostream>
#include <string>
#include "reply_admin.h"

using namespace std;

bool replyOperation(ReplyAdmin *_replyAdmin){
	string inputs;
	getline(cin, inputs);

	if(inputs == "#quit") return false;	///종료
	else if(inputs.find("#remove") != string::npos){
		inputs.erase(0, 8);

		int start = -1;
		int end = -1;
		int number = -1;

		int* numbers = new int[NUM_OF_CHAT];	//지워야할 번호의 배열을 저장한다
		int numbersCount = 0;

		bool isRemoveValid = false;

		for(int i=0; i<inputs.size(); ++i){
			char c = inputs[i];	//남은 inputs라는 string에서 정보를 가져온다

			if(isdigit(c) == true){
				int num = (int)c - (int)'0';
				if(number < 0) number = num;
				else number = number * 10 + num;
			}
			else{
				if(c == ',') numbers[numbersCount++] = number;	//','가 쓰인 remove문에서는 지워야할 번호를 배열에 저장한다
				else if(c == '-') start = number;	//'-'가 쓰인 remove문에서는 처음 나온 숫자를 스타트로 놓는다
				number = -1;
			}
		}
		if(start >= 0) end = number;

		if(end >= start && start >= 0) isRemoveValid = _replyAdmin->removeChat(start, end);	//'-'가 쓰인 remove실행
		else if(numbersCount > 0){
			numbers[numbersCount++] = number; // final number
			isRemoveValid = _replyAdmin->removeChat(numbers, numbersCount);	//','가 쓰인 remove실행
		}
		else if(number >= 0) isRemoveValid = _replyAdmin->removeChat(number);	//숫자만 쓰인 remove실행

		if(isRemoveValid == true) _replyAdmin->printChat();	//remove가 성공적으로 되었다면 남은 댓글 출력

		delete[] numbers;
	}
	else if(_replyAdmin->addChat(inputs)) _replyAdmin->printChat();	//댓글 추가

	return true;
}

ReplyAdmin::ReplyAdmin(){
	chats = new string[NUM_OF_CHAT];
	addChat("Hello, Reply Administrator!");
	addChat("I will be a good programmer.");
	addChat("This class is awesome.");
	addChat("Professor Lim is wise.");
	addChat("Two TAs are kind and helpful.");
	addChat("I think male TA looks cool.");
}

ReplyAdmin::~ReplyAdmin(){}

int ReplyAdmin::getChatCount(){
	int i;
	for(i=0; i<NUM_OF_CHAT; ++i){
		string s = chats[i];
		if(s.empty() == true)	break;
	}
	return i;
}

bool ReplyAdmin::addChat(string _chat){
	int count=getChatCount();
	if(count >= NUM_OF_CHAT)	return false;	//댓글이 최대 갯수인 NUM_OF_CHAT(200)을 초과한다면 add는 실패
	chats[count]=_chat;
	return true;
}
bool ReplyAdmin::removeChat(int _index){
	int count=getChatCount();
	if(_index>=count)	return false;	//숫자가 댓글에 없는 번호라면 remove는 실패
	for(int i=_index;i<count-1;++i)	chats[i]=chats[i+1];
	chats[count-1]="";
	return true;
}

bool ReplyAdmin::removeChat(int *_indices, int _count){
	bool checking=false;
	int count=getChatCount();	//배열에 있는 숫자가 모두 다 댓글에 없는 번호들이라면 remove는 실패
	for(int i=0;i<_count;++i){
		if(_indices[i]<count){
			chats[_indices[i]]="";
			checking=true;
		}
	}
	for(int i=0;i<count-1;++i){
		for(int j=i+1;j<count;++j){
			if(chats[i]==""){
				if(chats[j]!=""){
					chats[i]=chats[j];
					chats[j]="";	
				}
			}
		}
	}
	return checking;
}

bool ReplyAdmin::removeChat(int _start, int _end){
	int count=getChatCount();
	if(_start>=count || _start<0)	return false;	//스타트가 댓글에 없는 번호라면 remove는 실패
	for(int i=_start;i<count;++i)	chats[i]=chats[i+_end-_start+1];
	return true;
}

void ReplyAdmin::printChat(){
	for(int i=0; i<(getChatCount()); ++i)	cout << i << " " << chats[i] << endl;
}

int main(void){
	ReplyAdmin *replyAdmin = new ReplyAdmin();

	while(replyOperation(replyAdmin)) { /* Nothing else to do. */ }

	delete replyAdmin;
	return 0;
}