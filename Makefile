minimal_fighter : reply_admin
	g++ -o minimal_fighter minimal_fighter.cc minimal_fighter.h

reply_admin : simple_int_set
	g++ -o reply_admin reply_admin.cc reply_admin.h

simple_int_set : binary_search
	g++ -o simple_int_set simple_int_set.cc simple_int_set.h

binary_search : binary_search.cc binary_search.h
	g++ -o binary_search binary_search.cc binary_search.h