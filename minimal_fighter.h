// minimal_fighter.h
// Implement your minimal_fighter.cc
//2015004057 김범수

#ifndef __hw03__minimal_fighter__
#define __hw03__minimal_fighter__

#include <iostream>

using namespace std;

enum EFighterStatus{
    Invalid = 0,
    
    Alive,
    Dead,
};

typedef enum EFighterStatus FighterStatus;

class MinimalFighter{
	private:
		int mHp;
		int mPower;
		FighterStatus mStatus;

	public:
		MinimalFighter():mHp(0), mPower(0), mStatus(Invalid){}
		MinimalFighter(int _hp, int _power):mHp(_hp), mPower(_power), mStatus(_hp>0?Alive:Dead){}

		int hp() const{return mHp;}
		int power() const{return mPower;}
		FighterStatus status() const{return mStatus;}

		void setHp(int _hp);

		void hit(MinimalFighter *_enemy);
		void attack(MinimalFighter *_target);
		void fight(MinimalFighter *_enemy);
};

void MinimalFighter::setHp(int _hp){
	mHp=_hp;
}

void MinimalFighter::hit(MinimalFighter *_enemy){
	if(this->mStatus==Alive && _enemy->mStatus==Alive){
		_enemy->setHp(_enemy->hp() - this->power());
		this->setHp(this->hp() - _enemy->power());
		if(this->hp() <= 0)	this->mStatus=Dead;
		if(_enemy->hp() <= 0)	_enemy->mStatus=Dead;
	}
}

void MinimalFighter::attack(MinimalFighter *_target){
	if(this->mStatus==Alive && _target->mStatus==Alive){
		_target->setHp(_target->hp() - this->power());
		this->mPower=0;
		if(this->hp() <= 0)	this->mStatus=Dead;
		if(_target->hp() <= 0)	_target->mStatus=Dead;
	}
}

void MinimalFighter::fight(MinimalFighter *_enemy){
	if(this->mStatus==Alive && _enemy->mStatus==Alive){
		while(this->hp() >0 && _enemy->hp() >0){
			_enemy->setHp(_enemy->hp() - this->power());
			this->setHp(this->hp() - _enemy->power());
		}
		if(this->hp() <= 0)	this->mStatus=Dead;
		if(_enemy->hp() <= 0)	_enemy->mStatus=Dead;
	}
}

#endif